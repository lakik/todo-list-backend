CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE todo (
    todo_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id UUID,
    title TEXT NOT NULL,
    color TEXT NOT NULL,
    description TEXT NOT NULL,
    is_finished BOOL NOT NULL DEFAULT FALSE,

    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    last_edited_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);
