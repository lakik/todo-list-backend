module lakik.ca/todo-backend

go 1.12

require (
	github.com/golang-migrate/migrate/v4 v4.7.0
	github.com/gorilla/mux v1.7.3
	github.com/jmoiron/sqlx v1.2.0
	github.com/namsral/flag v1.7.4-pre
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
)
